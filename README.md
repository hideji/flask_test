### 使い方

#### Requirements

* Docker
* jq

#### 使い方

Docker build & run

```
$ sudo docker build -t py/flask_app .
$ sudo docker run -p 5000:5000 py/flask_app
```

curlから動作確認

```
$ BASE_URL='http://localhost:5000/api'
$ CALC="100 + 200 * 5"
$ UUID=$(curl ${BASE_URL}/uuid|jq '.Result.uuid')
$ curl -H 'Content-Type:application/json' -d '{"uuid":'${UUID}',"numericalFormula":"'"${CALC}"'"}' ${BASE_URL}/v1/calc 
$ curl localhost:5000/history

```


### テスト & 開発

#### Requirements

* Python 3.6
* poetry
* venv

#### 環境設定

```
$ python3.6 -m venv .venv
$ source .venv/bin/activate
$ poetry install
```

#### 実行 & テスト

実行

```
$ cd flask_app
$ python app.py
```

テスト

```
$ cd flask_app
$ python app_test.py
```