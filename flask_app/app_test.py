import app
import json
import unittest


class HelloTestCase(unittest.TestCase):

    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()
        app.db.create_all()

    def tearDown(self):
        app.db.session.remove()
        app.db.drop_all()

    # uuid
    def uuid(self):
        return self.app.get('/api/uuid')

    # calc
    def calc(self, v, type='application/json'):
        response = self.uuid()
        uuid = eval(response.data.decode('utf-8'))
        info = {"uuid": uuid['Result']['uuid'], "numericalFormula": v}
        return self.app.post('/api/v1/calc',
                             data=json.dumps(info),
                             headers={'Content-Type': type})

    # history
    def history(self):
        return self.app.get('/history')

    def test_top(self):
        response = self.app.get('/')
        html = response.data.decode('utf-8')
        self.assertIn('TODO：CREATE HTML Pag', html)

    def test_uuid(self):
        response = self.uuid()
        body = eval(response.data.decode('utf-8'))
        self.assertEqual(4, body['Result']['uuid'].count('-'))

    def test_calc1(self):
        response = self.calc('3+5')
        body = eval(response.data.decode('utf-8'))
        self.assertEqual(8, body['Result'])

    def test_calc2(self):
        # 電卓的には(3+5)*9
        response = self.calc('3+5*9/9/9')
        body = eval(response.data.decode('utf-8'))
        self.assertEqual((8/9), body['Result'])

    def test_calc3(self):
        response = self.calc('10')
        body = eval(response.data.decode('utf-8'))
        self.assertEqual(10, body['Result'])

    def test_error1(self):
        response = self.calc('3a+5*9')
        body = eval(response.data.decode('utf-8'))
        self.assertEqual('400 BAD REQUEST', response.status)
        self.assertEqual('bad numerical formula', body["error"])

    def test_error2(self):
        response = self.calc('3a+5*9', 'text/json')
        body = eval(response.data.decode('utf-8'))
        self.assertEqual('400 BAD REQUEST', response.status)
        self.assertEqual('Content Type Error', body["error"])

    def test_error3(self):
        response = self.calc('3++5*9')
        body = eval(response.data.decode('utf-8'))
        self.assertEqual('400 BAD REQUEST', response.status)
        self.assertIn('bad numerical formula', body["error"])

    def test_error4(self):
        response = self.calc('3+')
        body = eval(response.data.decode('utf-8'))
        self.assertEqual('400 BAD REQUEST', response.status)
        self.assertIn('bad numerical formula', body["error"])

    def test_error4(self):
        response = self.calc('3%2')
        body = eval(response.data.decode('utf-8'))
        self.assertEqual('400 BAD REQUEST', response.status)
        self.assertIn('bad numerical formula', body["error"])


    def test_history1(self):
        response = self.history()
        body = eval(response.data.decode('utf-8'))
        self.assertEqual(0, len(body['Result']))

    def test_history2(self):
        self.calc('3+5*9')
        self.calc('1+2+3+4+5+6')
        response = self.history()
        body = eval(response.data.decode('utf-8'))
        self.assertEqual(2, len(body['Result']))


if __name__ == '__main__':
    unittest.main()
