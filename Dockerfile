#PythonのDockerイメージを指定
FROM python:3.6

#ファイルをappディレクトリに追加
COPY flask_app/app.py /app/

#ルートディレクトリ設定
WORKDIR /app

#ライブラリインストール
RUN pip install flask
#RUN pip install Flask-Restless
RUN pip install Flask-SQLAlchemy

EXPOSE 5000

#コマンド実行
CMD ["python", "app.py"]