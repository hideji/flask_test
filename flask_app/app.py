from flask import Flask, request, jsonify, abort
import uuid
from datetime import datetime
from functools import wraps
import re

memo_uuid = {}

app = Flask(__name__)

from flask_sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///calc.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


# Model
class CalcResult(db.Model):
    __tablename__ = 'calc_results'                          # テーブル名
    uuid = db.Column(db.String(36), primary_key=True)       # カラム１(uuid)
    numericalFormula = db.Column(db.Text)                   # カラム2(numericalFormula)
    result = db.Column(db.Integer)                          # カラム3(result)
    create_uuid_at = db.Column(db.Float)                    # カラム４(create_uuid_at) UUIDを作成したUnix Time
    update_at = db.Column(db.DateTime, default=datetime.now())    # カラム5(update_at) デフォルト現在日時を設定

    def __init__(self, uuid, numericalFormula, result, create_uuid_at, date=None):
        self.uuid = uuid
        self.numericalFormula = numericalFormula
        self.result = result
        self.create_uuid_at = create_uuid_at
        self.date = date

    def to_dict(self):
        return dict(
            uuid=self.uuid,
            numericalFormula=self.numericalFormula,
            reuslt=self.result
        )

    def __repr__(self):
        return '<UUID %r>' % (self.uuid)


# Validator

def json_abort(status_code, data=None):
    if data is None:
        data = {}
    response = jsonify(data)
    response.status_code = status_code
    abort(response)


def checkContent(content_type):
    def _checkContent(function):
        @wraps(function)
        def __checkContent(*argv, **keywords):
            if request.headers['Content-Type'] != content_type:
                json_abort(400, {'error': 'Content Type Error'})
            return function(*argv, **keywords)
        return __checkContent
    return _checkContent


def checkInputValue():
    def _checkInputValue(function):
        @wraps(function)
        def __checkInputValue(*argv, **keywords):
            req_json = request.get_json()

            if ('uuid' not in req_json.keys()) or ('numericalFormula' not in req_json.keys()):
                json_abort(400, {'error': 'bad request'})

            # uuid check
            if req_json['uuid'] not in memo_uuid.keys():
                json_abort(400, {'error': 'uuid not found'})

            # 文字種チェック
            numop_reg = re.compile(r'[^0-9+-\\* ]+')
            if len(numop_reg.findall(req_json['numericalFormula'])) != 0:
                json_abort(400, {'error': 'bad numerical formula'})

            return function(*argv, **keywords)
        return __checkInputValue
    return _checkInputValue


# Routing
@app.route("/", methods=['GET'])
def web():
    return "TODO：CREATE HTML Page"


@app.route("/api/uuid", methods=['GET'])
def getuid():
    result = __create_uuid()
    return jsonify(result), 200


@app.route("/history", methods=['GET'])
def history():
    ls = CalcResult.query.all()
    if len(ls) == 0:
        return jsonify({'Result': []}), 200
    else:
        ls = [l.to_dict() for l in ls]
        return jsonify({'Result': ls}), 200


@app.route("/api/v1/calc", methods=['POST'])
@checkContent('application/json')
@checkInputValue()
def calc():
    req_json = request.get_json()
    result = __calculate(req_json['numericalFormula'])
    utime = memo_uuid.pop(req_json['uuid'])
    d = CalcResult(req_json['uuid'],  req_json['numericalFormula'], result, utime)
    db.session.add(d)
    db.session.commit()

    return jsonify({'Result': result}), 200


def __calculate(expression):
    op = ''
    righthand_value = ''
    # 空文字除去
    exp = expression.replace(' ', '')

    if len(exp) == 0:
        # ex. ' '
        return 0

    end_match_object = re.search('[0-9]+$', exp)
    if end_match_object is None:
        json_abort(400, {'error': "bad numerical formula:{}".format(exp)})

    match_object = re.match('^[0-9]+', exp)
    if match_object is None:
        json_abort(400, {'error': "bad numerical formula:{}".format(exp)})

    lefthand = match_object.group()
    for n in exp[match_object.end():]:
        if n in '+-/*':
            if len(op) != 0:
                # ex. ++
                json_abort(400, {'error': "bad numerical formula:{}".format(exp)})
            if len(righthand_value) != 0:
                lefthand = str(eval(lefthand + righthand_value))
                righthand_value = ''
            op = n
        else:
            if len(op) != 0:
                lefthand += op
            righthand_value += n
            op = ''

    return eval(lefthand + righthand_value)


def __create_uuid():
    uid = uuid.uuid4()
    now = datetime.now()
    now_unix = now.timestamp()
    memo_uuid[str(uid)] = now_unix
    result = {
        "Result": {
            "uuid": str(uid)
        }
    }
    return result


if __name__ == "__main__":
    db.drop_all()
    db.create_all()
    print('run start')
    app.run(host='0.0.0.0', port=5000)


